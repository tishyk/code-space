# -*- coding:utf-8 -*-

x1 = float( raw_input(u"Точка начала отрезка: " ) )
x2 = float( raw_input(u"Точка конца отрезка: " ) )
step = float( raw_input(u"Шаг: " ) )

if x1 > x2:
	x1, x2 = x2, x1
	

print(u"Функция: y = -3x**2 - 4x + 20")
print("%7s %7s"%('x','y'))

while x1 <= x2:
	y = -7*x1**2 - 2*x1 + 15
	print'%7.5s | %7.7s' % (x1, y)  # % 5 symbols, 2 symbols after dot
	x1 += step
