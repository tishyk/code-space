# -*- coding:utf-8 -*-

import pickle

data = {
    'a': [1, 2.0, 3, 4+6j],
    'b': ("character string", b"byte string"),
    'c': set([None, True, False])
}

with open('data.pickle', 'wb') as f:
    pickle.dump(data, f)

with open('data.pickle', 'rb') as f:
    data_new = pickle.load(f)

print(data_new)


s = pickle.dumps(data)
print s
data['d'] = 'abcdefg'
print data
c = pickle.loads(s)
print c


