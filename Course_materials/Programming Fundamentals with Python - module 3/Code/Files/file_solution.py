#! coding: utf-8

import os
import time

items = os.listdir('../..')
header_line = "%.2s\t%25.25s\t%.10s\t%.25s"% tuple("# ITEM TYPE CREATION_TIME".split())
print header_line
count=1

var_file = open('report.txt','a+')

for item in items:
    date = time.ctime(os.path.getctime('../../'+item))
    if os.path.isdir('../../'+item):
        item_type = "<DIR>"
    else:
        item_type = "<File>"
    line = "%s  %30.40s\t%.10s\t%.25s"%(count,item,item_type,date)
    print line
    var_file.write(line+'\n')
    var_file.flush()
    count+=1

var_file.close()
