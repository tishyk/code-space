# -*- coding: utf-8 -*-
import xlrd
import xlwt
from xlutils.copy import copy

file_name = 'example.xls'
rb = xlrd.open_workbook(file_name,on_demand=True,formatting_info=True)
wb = copy(rb)
wb.save("final_complete.xls")

#print help(wb)
