file_path = "apache_jmeter_access_log.txt"

log_dict = {}  # Expected structure {"http:\\...." : open(log_name, "a")}

for line in open(file_path).readlines()[1:]:
    link = line.split()[0]
    log_name = link.split(".")[0]
    log_name = log_name.split("//")[-1]

    log_file = log_dict.setdefault(link, open("Logs1/{}.txt".format(log_name), "a"))
    log_file.write("{}\n".format(link))
    log_file.close()

print("Done 1 parsing")











log_dict.clear()  # Expected structure {"file_name" : "links"}

for line in open(file_path).readlines()[1:]:
    link = line.split()[0] + "\n"
    log_name = link.split(".")[0]
    log_name = log_name.split("//")[-1]

    if link not in log_dict.setdefault(log_name, link):
        log_dict[log_name] = log_dict[log_name] + link

for log_name, links in log_dict.items():
    log_file = open("Logs2/{}.txt".format(log_name), "w")
    log_file.write(links)

print("Done 2 parsing")
