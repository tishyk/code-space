import os
file_path="Homework.txt"
weather_dir = "weather"
block_data = {}
file = open(file_path, encoding="utf8")

if not os.path.exists(weather_dir):
    os.mkdir(weather_dir)


def get_block_data(data):
    file_name = ""
    for line in data:
        # Привет
        if u"Tемпература воздуха, °C" in line:
            file_name = line.split("\t")[0]
            file_name = file_name.replace(",","")
            file_name = file_name.replace('"', '')
            file_name = file_name.replace(" ","_")
            block_data[file_name] = line
        else:
            block_data[file_name] += line

 
def write_file(file_name, file_data):
    with open(file_name, "w", encoding="utf8") as weather_file:
        weather_file.write(file_data)

get_block_data(file)
for date in block_data:
    write_file("{}/{}".format(weather_dir,date), block_data[date])
