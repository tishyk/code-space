import os
dirs = ["Introduction into Python language",
        "Syntax of the language and built-in methods",
        "Basic object types",
        "Functions",
        "Built-in functions",
        "Deep into the functional programming",
        "Module",
        "System variables processing. Interacting with the operating system",
        "Exceptions"]
name = "Programming Fundamentals with Python"

for index, d in enumerate(dirs):
    _name = "{} - module {}".format(name,index+1)
    print(_name)
    # make dir for module
    # os.popen('mkdir "%s"'%_name)
    # Make pptx files for presentation materials
    # open("{0}/{0}.pptx".format(_name),"w").close()
    # Make labwork directory for all modules
    os.popen('mkdir "{0}/Laboratory work - module {1}"'.format(_name,index+1))
        

    
