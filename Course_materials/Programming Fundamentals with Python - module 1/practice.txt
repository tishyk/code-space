>>> "kjsbvjsb
SyntaxError: EOL while scanning string literal

>>>     "zczvz"
    
SyntaxError: unexpected indent

>>> if True:
"Hello World!"
SyntaxError: expected an indented block

>>> "Hello World!" ; "Hi!"
'Hello World!'
'Hi!'

>>> "Hello World!":
	
SyntaxError: invalid syntax

>>> Hello World!
SyntaxError: invalid syntax

>>> print(Hello World!)
SyntaxError: invalid syntax


>>> if True:
	print("Hello World!")

	
Hello World!

Set this instruction with IDLE Shell, IDLE Editor and cmd or bash in interactive mode.