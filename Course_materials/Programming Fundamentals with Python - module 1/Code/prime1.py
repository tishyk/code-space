def prime(a):		
    if a == 2: return True					
    if a < 2 or a % 2 == 0: return False	
    return not any(a % x == 0 for x in range(3, int(a**0.5) + 1, 2)
	
# Calling function above and asking for enter. Or you can replace the 'input()' with your value explicitly
prime(input())