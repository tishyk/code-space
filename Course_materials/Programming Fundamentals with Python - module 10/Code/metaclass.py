class MyMetaClass(type):
    def __new__(cls, name, bases, namespace, **kwds):
        name += '_by_MyMetaClass'
        return type(name, bases, kwds)

    def __init__(self, **kwargs):
        [setattr(MyMetaClass, name, kwargs[name]) for name in kwargs]


class MyClass():#metaclass=MyMetaClass):
    __metaclass__ = MyMetaClass
    pass


print(MyClass.__name__)
# out: MyClass_by_MyMetaClass
