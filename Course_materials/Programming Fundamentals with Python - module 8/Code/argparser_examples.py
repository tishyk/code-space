import argparse
'''
parser = argparse.ArgumentParser()

parser.add_argument('-cont', '--content', type=str, help="Choose content type for modify.")


parser.add_argument("-l", '--lang', type=str,
                    help="Choose language abbreviation! (Ex. 'ua','en')")

parser.add_argument("-c", '--keyword', type=str, default='help',
                    help="Choose word for filter! (Ex. 'hello')")

parser.add_argument("-m", '--value', type=int, default=0,
                    help="""Choose number of the best moody for tweets!
                    (Ex. '10', '-12')""")


args = parser.parse_args()

lang = args.lang
keyword = args.keyword
moody_arg = args.value

if __name__ == '__main__':
    print('Parser initialized! Please wait for filtering..')
    print(lang, keyword, moody_arg)

'''





# Mutually exclusive group

parser = argparse.ArgumentParser()

option_group = parser.add_mutually_exclusive_group(required=True)

option_group.add_argument("-ca", "--video", action="store_true", default=False,
                          help="Clear all data from uServer.conf file")
option_group.add_argument("-go", "--image", action="store_true", default=False,
                          help="Remove all data from file except GUID")
option_group.add_argument("-do", "--audio", action="store_true", default=False,
                          help="Remove all data from file except disks S/N")

args = parser.parse_args()

print(args.image)

"""
        names = [
            'option_strings',
            'dest',
            'nargs',
            'const',
            'default',
            'type',
            'choices',
            'help',
            'metavar',
            ]


        # register actions
        self.register('action', None, _StoreAction)
        self.register('action', 'store', _StoreAction)
        self.register('action', 'store_const', _StoreConstAction)
        self.register('action', 'store_true', _StoreTrueAction)
        self.register('action', 'store_false', _StoreFalseAction)
        self.register('action', 'append', _AppendAction)
        self.register('action', 'append_const', _AppendConstAction)
        self.register('action', 'count', _CountAction)
        self.register('action', 'help', _HelpAction)
        self.register('action', 'version', _VersionAction)
        self.register('action', 'parsers', _SubParsersAction)
"""
'''
