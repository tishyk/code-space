def func(a, *args, z=10, **kwargs):
    print(a)
    print(z)
    print(args)
    print(kwargs)
    print("-"*80)

#func(1,2,3,4,5,6)
#func(1,2,3,4, z=15, b=20, c=30)

def func(a, b, c):
    print(type(a), a)
    print(type(b), b)
    print(type(c), c)

func(10,c ="text", b=20)
