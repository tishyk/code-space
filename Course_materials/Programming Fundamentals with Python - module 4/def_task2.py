# -*- coding: utf-8 -*-

start = input(u'Точка начала отрезка: ')
end = input(u'Точка конца отрезка: ')
step = input(u'Шаг: ')
if start > end:
    start, end = end, start
x = start
print ' x\ty'

while start <= x <= end:
    y = -3*x*x - 4*x + 20
    print round(x, 2), '|', round(y, 2)
    x += step
