
def func_name(value):
	print value

func_name('some object')

#----------------------------------
def func_name1(value='some object'):
	print value
	
func_name1()	
func_name1('some object2')

#----------------------------------
def func_name2(*value):
	print value

	
func_name2(1,2,3,'abc',[1.1,2,3])
func_name2()

#----------------------------------
def func_name3(**value):
	print value

func_name3(a=1,b=2)
func_name3(line='value')

#----------------------------------
def func_name4(value,default_value=10,*args,**kwargs):
	print value,default_value,args,kwargs

func_name4(15,15,11,12,13,14,a=112,b=220)

#----------------------------------
def func_name5(value1,value2):
	print value1,value2

func_name5(value2=15,value1=10)

#----------------------------------
l = [1,2,3,'a','b']
def func_name6(value1):
	print value1

func_name6(l)
#func_name6(*l) # TypeError: func_name6() takes exactly 1 argument (5 given)

def func_name6(a,b,c=10,*value1):
	print a,b,c,value1
	
func_name6(*l)


d = {'a':10,'c':20,'b':'abc'}
def func_name6(a,b,c):
	print a,b,c

d = {'a':10,'c':20,'b':'abc','e':1022}
#func_name6(**d)
def func_name6(a,b,c,*args):
	print a,b,c,args
func_name6(*d) # a c b ('e',)	
