#! coding: utf-8
import time
import random
import string

lst = ['a','b','c']

print max(lst)
print max(lst, key=len)
print min(lst, key=len)

lst_int = [1,2,3,4]
print sum(lst_int)

lst_bool = [True,True,False]
print any(lst_bool)
print all(lst_bool)
print all(lst)

print round(random.random()*10,2)
print random.choice('abcder')

lst_letters = string.ascii_lowercase
print random.choice(lst_letters)


for item in enumerate(lst):
    print "Index: %s Value: %s"%item


def my_func(lst):
    for item in lst:
        return item


item = my_func()
print item
item = my_func()
print item
item = my_func()
print item


def my_enumerate(lst):
    for item in lst:
        yield lst.index(item)+1,item
        
      
for i in my_enumerate(lst):
    print i
    

lst = ['a','b','c']

def my_iter(lst):
    for item in lst:
        yield lst.index(item), item
       
def my_iter(lst,a=[]):
    for item in lst:
        a.append(1)
        yield len(a)-1, item


my_enumerate = my_iter
time.clock()

for i,x in my_enumerate(lst):
    print "Index: %s Value: %s"%(i,x)
    
print time.clock()


lst_letters = string.ascii_lowercase
lst_int = range(50)

print lst_letters
print lst_int



def func(a,b):
    l = []
    for e,i in enumerate(b):
        l.append((i,b[e]))
    return l

#print func(lst_int,lst_letters)
        
        









