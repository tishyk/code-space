import random
import string

dir_len_argument = 8

def files(num=''):
    # Generate random file names quantity
    if not num:
        files_q = random.sample(string.ascii_lowercase*4, random.randint(0,30))
    else:
        files_q = random.sample(string.ascii_lowercase*400000, num)
    return [i for i in set(files_q)]

def dir_names(f_names, dir_len):
    # Expected result e)xample['a-c', 'd-g', 'n-z']
    antje = len(f_names)//dir_len  # number of folders
    mantissa = len(f_names) % dir_len # additional folders
    folder_names = []
    step = 1
    frame = [0,0]
    print(f_names, antje, mantissa, end=" ===> ")
    for e, names in enumerate(f_names[::step]):
        step = 0
        if e >=frame[1]:
            if mantissa:
                frame = (e+step, e+antje+1)
                mantissa-=1
            else:
                frame = (e+step, e+antje)
            #print(frame)
            step = frame[-1]
            folder_name = f_names[frame[0]:frame[1]]
            folder_names.append(folder_name)
    print(folder_names)

dir_names(sorted(files()) ,dir_len_argument)
#dir_names(sorted(files()) ,0) # ZeroDivisionError
dir_names(sorted(files()) ,2)
dir_names(sorted(files()) ,100)
dir_names(sorted(files(1000000)) ,20000)
dir_names(sorted(files()) ,12)
#dir_names(sorted(files()) ,'v') # TypeError

        
        
    
    
    
    


