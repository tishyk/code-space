import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-k', '--keyword', type=str, default='media',
                    help="Choose content type for modify.")

parser.add_argument("-c", '--count', type=int, default=5,
                    help='Choose number of the folder scaen depth')

parser.add_argument("-s", '--safe_delete', action="store_true", default=False,
                          help="Remove all duplicated data into trash")


def get_arguments():
    args = parser.parse_args()
    keyword = args.keyword
    count = args.count
    safe_delete = args.safe_delete
    return args,keyword,count,safe_delete
